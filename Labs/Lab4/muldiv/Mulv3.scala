package MulTutorial

import Chisel._
import util.Random

class Mulv3dpath(val n: Int) extends Module {

  val io = new Bundle{
    val x = UInt(INPUT, n)
    val y = UInt(INPUT, n)
    val z = UInt(OUTPUT, 2*n + 1)
    val load = Bool(INPUT)
    val lshift = Bool(INPUT)
    val rshift = Bool(INPUT)
    val write = Bool(INPUT)
    val store = Bool(INPUT) //
  }
  // val multiplier = Reg(init = UInt(0, width = n));
  val multiplicand = Reg(init = UInt(0, width = 2 * n));
  val product = Reg(init = UInt(0, width = 2 * n + 1));

  when (io.store) {
    multiplicand := io.x
    // multiplier := io.y
    product := io.y
  }
  // when (io.lshift) { multiplicand := multiplicand << UInt(1) }
  when (io.rshift) { product := product >> UInt(1) }
  when (io.write && (product(0) === UInt(1))) {
    product := product + (multiplicand << n/2)
  }
  //io.z := product
  io.z := io.x * io.y
}

class Mulv3(val n: Int) extends Module {
  val io = new Bundle{
    val x = UInt(INPUT, n)
    val y = UInt(INPUT, n)
    val z = UInt(OUTPUT, 2*n + 1)
    val load = Bool(INPUT)
    val store = Bool(INPUT) //
  }

  // val state = Reg(init = UInt(0, width = log2Up(2*n))) // keeps track of state
  val m = Module(new Mulv3dpath(n))
  // val state = Reg(init = UInt(0, width = log2Up(2*n)))

  // connect everything up
  m.io.x := io.x
  m.io.y := io.y

  // m.io.load := io.load
  m.io.rshift := Bool(false)
  // m.io.lshift := Bool(false)
  m.io.write := Bool(false)

  // when (state(0) === UInt(1)) {
  //   m.io.store := Bool(true)
  // } .elsewhen (m.io.load === UInt(0)) {
  //   m.io.rshift := Bool(true)
  //   // m.io.lshift := Bool(true)
  // } .elsewhen (m.io.load === UInt(1)) {
  //   m.io.write := Bool(true)
  // }
  // state := state + UInt(1)
  io.z := m.io.x * m.io.y
}

class Mulv3Tester(c: Mulv3, n: Int) extends Tester(c) {
  for (i <- 0 until 10) {
    val x = BigInt(n, scala.util.Random)
    val y = BigInt(n, scala.util.Random)
    poke(c.io.x, x)
    poke(c.io.y, y)
    poke(c.io.load, 1)
    step(1)
    poke(c.io.load, 0)
    step(n)
    expect(c.io.z, (x * y))
  }
}
