#ifndef __Bisc__
#define __Bisc__

#include "emulator.h"

class Bisc_t : public mod_t {
 private:
  val_t __rand_seed;
  void __srand(val_t seed) { __rand_seed = seed; }
  val_t __rand_val() { return ::__rand_val(&__rand_seed); }
 public:
  dat_t<1> Bisc_alu__io_op;
  dat_t<1> Bisc__io_reset;
  dat_t<1> Bisc__io_isWr;
  dat_t<1> T21;
  dat_t<1> T23;
  dat_t<1> reset;
  dat_t<2> Bisc__rd;
  dat_t<10> T36;
  dat_t<10> Bisc__io_wrAddr;
  dat_t<32> Bisc__io_iout;
  dat_t<32> Bisc__io_pcout;
  dat_t<32> Bisc_alu__io_b;
  dat_t<32> Bisc_alu__io_a;
  dat_t<32> Bisc_alu__io_out;
  dat_t<32> T35;
  dat_t<32> Bisc__pc;
  dat_t<32> Bisc__io_wrData;
  mem_t<32,4> Bisc__regfile;
  mem_t<32,1024> Bisc__imem;
  dat_t<1> Bisc_alu__io_op__prev;
  dat_t<1> Bisc__io_reset__prev;
  dat_t<1> Bisc__io_isWr__prev;
  dat_t<10> Bisc__io_wrAddr__prev;
  dat_t<32> Bisc__io_iout__prev;
  dat_t<32> Bisc__io_pcout__prev;
  dat_t<32> Bisc_alu__io_b__prev;
  dat_t<32> Bisc_alu__io_a__prev;
  dat_t<32> Bisc_alu__io_out__prev;
  dat_t<32> Bisc__pc__prev;
  dat_t<32> Bisc__io_wrData__prev;
  clk_t clk;
  dat_t<1> reset__prev;

  void init ( val_t rand_init = 0 );
  void clock_lo ( dat_t<1> reset, bool assert_fire=true );
  void clock_hi ( dat_t<1> reset );
  int clock ( dat_t<1> reset );
  void print ( FILE* f );
  void print ( std::ostream& s );
  void dump ( FILE* f, val_t t, dat_t<1> reset=LIT<1>(0) );
  void dump_init ( FILE* f );

};

#include "emul_api.h"
class Bisc_api_t : public emul_api_t {
 public:
  Bisc_api_t(mod_t* m) : emul_api_t(m) { }
  void init_sim_data();
};

#endif
