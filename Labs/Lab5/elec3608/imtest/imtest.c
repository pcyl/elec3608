// See LICENSE for license details.

// *************************************************************************
// multiply filter bencmark
// -------------------------------------------------------------------------
//
// This benchmark tests the software multiply implemenation. The
// input data (and reference data) should be generated using the
// multiply_gendata.pl perl script and dumped to a file named
// dataset1.h You should not change anything except the
// HOST_DEBUG and VERIFY macros for your timing run.

#include <stdio.h>
#include <stdlib.h>
#include "util.h"

#include "multiply.h"

//--------------------------------------------------------------------------
// Input/Reference Data

//--------------------------------------------------------------------------
// Main

int main( int argc, char* argv[] )
{
  int n = 5;
  int fact = 1;
  int i;
  


  setStats(1);

  for (i = 1; i <= n; i++)
  {
	printf("iteration %d\n",i);
	srand(i);
	  uint32_t u32a = ((rand())<<16)|(rand());
	  int32_t s32a = (int32_t)u32a;

	  uint64_t u64a = u32a;
	  //uint64_t sa = (int64_t) ((int32_t) u64a);

	  uint32_t u32b = ((rand())<<16)|(rand());
	 int32_t s32b = (int32_t)u32b;

	  uint64_t u64b = u32b;
	  //uint64_t sb = (int64_t) ((int32_t) u64b);

	  int64_t resa = (int64_t)s32a*s32b;
	  uint64_t resb = (uint64_t)u32a*u32b;

	  printf("resa = %lld\n", resa);
	  printf("resb = %llu\n", resb);

  }
  setStats(0);
  
  // Print out the results
 // printf("IM version factorial(%d)=", n, fact);
  if (fact == 3628800) {
    printf("%d (ok)\n", fact);
    exit(0);
  }
  else {
    printf("%d (fail)\n", fact);
    exit(10);
  }
}
