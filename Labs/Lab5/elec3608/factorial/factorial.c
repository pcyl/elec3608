// See LICENSE for license details.

// *************************************************************************
// multiply filter bencmark
// -------------------------------------------------------------------------
//
// This benchmark tests the software multiply implemenation. The
// input data (and reference data) should be generated using the
// multiply_gendata.pl perl script and dumped to a file named
// dataset1.h You should not change anything except the
// HOST_DEBUG and VERIFY macros for your timing run.

#include <stdio.h>
#include <stdlib.h>
#include "util.h"

#include "multiply.h"

//--------------------------------------------------------------------------
// Input/Reference Data

//--------------------------------------------------------------------------
// Main

int main( int argc, char* argv[] )
{
  int n = 10;
  int fact = 1;
  int i;

  setStats(1);
  for (i = 1; i <= n; i++)
  {
    fact = fact * i;
  }
  setStats(0);
  
  // Print out the results
  printf("I version factorial(%d)=", n, fact);
  if (fact == 3628800) {
    printf("%d (ok)\n", fact);
    exit(0);
  }
  else {
    printf("%d (fail)\n", fact);
    exit(10);
  }
}
