#include "Mul.h"

void Mul_t::init ( val_t rand_init ) {
  this->__srand(rand_init);
  Mul_m__multiplier.randomize(&__rand_seed);
  Mul_m__multiplicand.randomize(&__rand_seed);
  Mul_m__product.randomize(&__rand_seed);
  Mul__state.randomize(&__rand_seed);
  clk.len = 1;
  clk.cnt = 0;
  clk.values[0] = 0;
}


int Mul_t::clock ( dat_t<1> reset ) {
  uint32_t min = ((uint32_t)1<<31)-1;
  if (clk.cnt < min) min = clk.cnt;
  clk.cnt-=min;
  if (clk.cnt == 0) clock_lo( reset );
  if (!reset.to_bool()) print( std::cerr );
  mod_t::dump( reset );
  if (clk.cnt == 0) clock_hi( reset );
  if (clk.cnt == 0) clk.cnt = clk.len;
  return min;
}


void Mul_t::print ( FILE* f ) {
}
void Mul_t::print ( std::ostream& s ) {
}


void Mul_t::dump_init ( FILE* f ) {
  fputs("$timescale 1ps $end\n", f);
  fputs("$scope module Mul $end\n", f);
  fputs("$var wire 1 \x21 clk $end\n", f);
  fputs("$var wire 1 \x22 reset $end\n", f);
  fputs("$var wire 3 \x28 state $end\n", f);
  fputs("$var wire 4 \x29 io_y $end\n", f);
  fputs("$var wire 4 \x2c io_x $end\n", f);
  fputs("$var wire 8 \x31 io_z $end\n", f);
  fputs("$scope module m $end\n", f);
  fputs("$var wire 1 \x23 io_load $end\n", f);
  fputs("$var wire 1 \x24 io_rshift $end\n", f);
  fputs("$var wire 1 \x25 reset $end\n", f);
  fputs("$var wire 1 \x26 io_lshift $end\n", f);
  fputs("$var wire 1 \x27 io_write $end\n", f);
  fputs("$var wire 4 \x2a io_y $end\n", f);
  fputs("$var wire 4 \x2b multiplier $end\n", f);
  fputs("$var wire 4 \x2d io_x $end\n", f);
  fputs("$var wire 8 \x2e multiplicand $end\n", f);
  fputs("$var wire 8 \x2f product $end\n", f);
  fputs("$var wire 8 \x30 io_z $end\n", f);
  fputs("$upscope $end\n", f);
  fputs("$upscope $end\n", f);
  fputs("$enddefinitions $end\n", f);
  fputs("$dumpvars\n", f);
  fputs("$end\n", f);
  fputs("#0\n", f);
  if (clk.cnt == 0) {
    clk.values[0] = 1;
    dat_dump<1>(f, clk, 0x21);
  }
  dat_t<1> reset = LIT<1>(1);
  dat_dump<1>(f, reset, 0x22);
  dat_dump<1>(f, Mul_m__io_load, 0x23);
  Mul_m__io_load__prev = Mul_m__io_load;
  dat_dump<1>(f, Mul_m__io_rshift, 0x24);
  Mul_m__io_rshift__prev = Mul_m__io_rshift;
  dat_dump<1>(f, Mul_m__reset, 0x25);
  Mul_m__reset__prev = Mul_m__reset;
  dat_dump<1>(f, Mul_m__io_lshift, 0x26);
  Mul_m__io_lshift__prev = Mul_m__io_lshift;
  dat_dump<1>(f, Mul_m__io_write, 0x27);
  Mul_m__io_write__prev = Mul_m__io_write;
  dat_dump<1>(f, Mul__state, 0x28);
  Mul__state__prev = Mul__state;
  dat_dump<1>(f, Mul__io_y, 0x29);
  Mul__io_y__prev = Mul__io_y;
  dat_dump<1>(f, Mul_m__io_y, 0x2a);
  Mul_m__io_y__prev = Mul_m__io_y;
  dat_dump<1>(f, Mul_m__multiplier, 0x2b);
  Mul_m__multiplier__prev = Mul_m__multiplier;
  dat_dump<1>(f, Mul__io_x, 0x2c);
  Mul__io_x__prev = Mul__io_x;
  dat_dump<1>(f, Mul_m__io_x, 0x2d);
  Mul_m__io_x__prev = Mul_m__io_x;
  dat_dump<1>(f, Mul_m__multiplicand, 0x2e);
  Mul_m__multiplicand__prev = Mul_m__multiplicand;
  dat_dump<1>(f, Mul_m__product, 0x2f);
  Mul_m__product__prev = Mul_m__product;
  dat_dump<1>(f, Mul_m__io_z, 0x30);
  Mul_m__io_z__prev = Mul_m__io_z;
  dat_dump<1>(f, Mul__io_z, 0x31);
  Mul__io_z__prev = Mul__io_z;
  fputs("#1\n", f);
  if (clk.cnt == 0) {
    clk.values[0] = 0;
    dat_dump<1>(f, clk, 0x21);
  }
}


void Mul_t::dump ( FILE* f, val_t t, dat_t<1> reset ) {
  if (t == 0L) return dump_init(f);
  fprintf(f, "#%lu\n", t << 1);
  if (clk.cnt == 0)  goto L0;
K0:  if (reset != reset__prev)  goto L1;
K1:  if (Mul_m__io_load != Mul_m__io_load__prev)  goto L2;
K2:  if (Mul_m__io_rshift != Mul_m__io_rshift__prev)  goto L3;
K3:  if (Mul_m__reset != Mul_m__reset__prev)  goto L4;
K4:  if (Mul_m__io_lshift != Mul_m__io_lshift__prev)  goto L5;
K5:  if (Mul_m__io_write != Mul_m__io_write__prev)  goto L6;
K6:  if (Mul__state != Mul__state__prev)  goto L7;
K7:  if (Mul__io_y != Mul__io_y__prev)  goto L8;
K8:  if (Mul_m__io_y != Mul_m__io_y__prev)  goto L9;
K9:  if (Mul_m__multiplier != Mul_m__multiplier__prev)  goto L10;
K10:  if (Mul__io_x != Mul__io_x__prev)  goto L11;
K11:  if (Mul_m__io_x != Mul_m__io_x__prev)  goto L12;
K12:  if (Mul_m__multiplicand != Mul_m__multiplicand__prev)  goto L13;
K13:  if (Mul_m__product != Mul_m__product__prev)  goto L14;
K14:  if (Mul_m__io_z != Mul_m__io_z__prev)  goto L15;
K15:  if (Mul__io_z != Mul__io_z__prev)  goto L16;
K16:  fprintf(f, "#%lu\n", (t << 1) + 1);
  if (clk.cnt == 0)  goto Z0;
C0:  return;
L0:
  clk.values[0] = 1;
  dat_dump<1>(f, clk, 0x21);
  goto K0;
L1:
  reset__prev = reset;
  dat_dump<1>(f, reset, 0x22);
  goto K1;
L2:
  Mul_m__io_load__prev = Mul_m__io_load;
  dat_dump<1>(f, Mul_m__io_load, 0x23);
  goto K2;
L3:
  Mul_m__io_rshift__prev = Mul_m__io_rshift;
  dat_dump<1>(f, Mul_m__io_rshift, 0x24);
  goto K3;
L4:
  Mul_m__reset__prev = Mul_m__reset;
  dat_dump<1>(f, Mul_m__reset, 0x25);
  goto K4;
L5:
  Mul_m__io_lshift__prev = Mul_m__io_lshift;
  dat_dump<1>(f, Mul_m__io_lshift, 0x26);
  goto K5;
L6:
  Mul_m__io_write__prev = Mul_m__io_write;
  dat_dump<1>(f, Mul_m__io_write, 0x27);
  goto K6;
L7:
  Mul__state__prev = Mul__state;
  dat_dump<1>(f, Mul__state, 0x28);
  goto K7;
L8:
  Mul__io_y__prev = Mul__io_y;
  dat_dump<1>(f, Mul__io_y, 0x29);
  goto K8;
L9:
  Mul_m__io_y__prev = Mul_m__io_y;
  dat_dump<1>(f, Mul_m__io_y, 0x2a);
  goto K9;
L10:
  Mul_m__multiplier__prev = Mul_m__multiplier;
  dat_dump<1>(f, Mul_m__multiplier, 0x2b);
  goto K10;
L11:
  Mul__io_x__prev = Mul__io_x;
  dat_dump<1>(f, Mul__io_x, 0x2c);
  goto K11;
L12:
  Mul_m__io_x__prev = Mul_m__io_x;
  dat_dump<1>(f, Mul_m__io_x, 0x2d);
  goto K12;
L13:
  Mul_m__multiplicand__prev = Mul_m__multiplicand;
  dat_dump<1>(f, Mul_m__multiplicand, 0x2e);
  goto K13;
L14:
  Mul_m__product__prev = Mul_m__product;
  dat_dump<1>(f, Mul_m__product, 0x2f);
  goto K14;
L15:
  Mul_m__io_z__prev = Mul_m__io_z;
  dat_dump<1>(f, Mul_m__io_z, 0x30);
  goto K15;
L16:
  Mul__io_z__prev = Mul__io_z;
  dat_dump<1>(f, Mul__io_z, 0x31);
  goto K16;
Z0:
  clk.values[0] = 0;
  dat_dump<1>(f, clk, 0x21);
  goto C0;
}




void Mul_t::clock_lo ( dat_t<1> reset, bool assert_fire ) {
  { Mul_m__io_y.values[0] = Mul__io_y.values[0];}
  val_t T0;
  T0 = Mul__state.values[0] == 0x0L;
  { Mul_m__io_load.values[0] = T0;}
  val_t T1;
  { T1 = TERNARY_1(Mul_m__io_load.values[0], Mul_m__io_y.values[0], Mul_m__multiplier.values[0]);}
  val_t T2;
  T2 = Mul_m__multiplier.values[0] >> 0x1L;
  val_t T3;
  { T3 = T2 | 0x0L << 3;}
  val_t T4;
  T4 = (Mul__state.values[0] >> 0) & 1;
  val_t T5;
  T5 = T4 == 0x0L;
  val_t T6;
  { T6 = T0 ^ 0x1L;}
  val_t T7;
  { T7 = T6 & T5;}
  { Mul_m__io_rshift.values[0] = T7;}
  val_t T8;
  { T8 = TERNARY_1(Mul_m__io_rshift.values[0], T3, T1);}
  { Mul_m__reset.values[0] = reset.values[0];}
  { T9.values[0] = TERNARY(Mul_m__reset.values[0], 0x0L, T8);}
  { Mul_m__io_x.values[0] = Mul__io_x.values[0];}
  val_t T10;
  { T10 = Mul_m__io_x.values[0] | 0x0L << 4;}
  val_t T11;
  { T11 = TERNARY_1(Mul_m__io_load.values[0], T10, Mul_m__multiplicand.values[0]);}
  val_t T12;
  { T12 = T11 | 0x0L << 8;}
  val_t T13;
  T13 = Mul_m__multiplicand.values[0] << 0x1L;
  T13 = T13 & 0x1ffL;
  { Mul_m__io_lshift.values[0] = T7;}
  val_t T14;
  { T14 = TERNARY_1(Mul_m__io_lshift.values[0], T13, T12);}
  val_t T15;
  { T15 = TERNARY(Mul_m__reset.values[0], 0x0L, T14);}
  { T16.values[0] = T15;}
  T16.values[0] = T16.values[0] & 0xffL;
  val_t T17;
  { T17 = TERNARY(Mul_m__io_load.values[0], 0x0L, Mul_m__product.values[0]);}
  val_t T18;
  { T18 = Mul_m__product.values[0]+Mul_m__multiplicand.values[0];}
  T18 = T18 & 0xffL;
  val_t T19;
  T19 = (Mul_m__multiplier.values[0] >> 0) & 1;
  val_t T20;
  T20 = T19 == 0x1L;
  val_t T21;
  T21 = (Mul__state.values[0] >> 0) & 1;
  val_t T22;
  T22 = T21 == 0x1L;
  val_t T23;
  { T23 = T0 | T5;}
  val_t T24;
  { T24 = T23 ^ 0x1L;}
  val_t T25;
  { T25 = T24 & T22;}
  { Mul_m__io_write.values[0] = T25;}
  val_t T26;
  { T26 = Mul_m__io_write.values[0] & T20;}
  val_t T27;
  { T27 = TERNARY_1(T26, T18, T17);}
  { T28.values[0] = TERNARY(Mul_m__reset.values[0], 0x0L, T27);}
  { Mul_m__io_z.values[0] = Mul_m__product.values[0];}
  { Mul__io_z.values[0] = Mul_m__io_z.values[0];}
  val_t T29;
  { T29 = Mul__state.values[0]+0x1L;}
  T29 = T29 & 0x7L;
  { T30.values[0] = TERNARY(reset.values[0], 0x0L, T29);}
}


void Mul_t::clock_hi ( dat_t<1> reset ) {
  dat_t<4> Mul_m__multiplier__shadow = T9;
  dat_t<8> Mul_m__multiplicand__shadow = T16;
  dat_t<8> Mul_m__product__shadow = T28;
  dat_t<3> Mul__state__shadow = T30;
  Mul_m__multiplier = T9;
  Mul_m__multiplicand = T16;
  Mul_m__product = T28;
  Mul__state = T30;
}


void Mul_api_t::init_sim_data (  ) {
  sim_data.inputs.clear();
  sim_data.outputs.clear();
  sim_data.signals.clear();
  Mul_t* mod = dynamic_cast<Mul_t*>(module);
  assert(mod);
  sim_data.inputs.push_back(new dat_api<4>(&mod->Mul__io_x));
  sim_data.inputs.push_back(new dat_api<4>(&mod->Mul__io_y));
  sim_data.outputs.push_back(new dat_api<8>(&mod->Mul__io_z));
  sim_data.signals.push_back(new dat_api<4>(&mod->Mul_m__io_y));
  sim_data.signal_map["Mul.m.io_y"] = 0;
  sim_data.signals.push_back(new dat_api<1>(&mod->Mul_m__io_load));
  sim_data.signal_map["Mul.m.io_load"] = 1;
  sim_data.signals.push_back(new dat_api<1>(&mod->Mul_m__io_rshift));
  sim_data.signal_map["Mul.m.io_rshift"] = 2;
  sim_data.signals.push_back(new dat_api<1>(&mod->Mul_m__reset));
  sim_data.signal_map["Mul.m.reset"] = 3;
  sim_data.signals.push_back(new dat_api<4>(&mod->Mul_m__multiplier));
  sim_data.signal_map["Mul.m.multiplier"] = 4;
  sim_data.signals.push_back(new dat_api<4>(&mod->Mul_m__io_x));
  sim_data.signal_map["Mul.m.io_x"] = 5;
  sim_data.signals.push_back(new dat_api<1>(&mod->Mul_m__io_lshift));
  sim_data.signal_map["Mul.m.io_lshift"] = 6;
  sim_data.signals.push_back(new dat_api<8>(&mod->Mul_m__multiplicand));
  sim_data.signal_map["Mul.m.multiplicand"] = 7;
  sim_data.signals.push_back(new dat_api<1>(&mod->Mul_m__io_write));
  sim_data.signal_map["Mul.m.io_write"] = 8;
  sim_data.signals.push_back(new dat_api<8>(&mod->Mul_m__product));
  sim_data.signal_map["Mul.m.product"] = 9;
  sim_data.signals.push_back(new dat_api<8>(&mod->Mul_m__io_z));
  sim_data.signal_map["Mul.m.io_z"] = 10;
  sim_data.signals.push_back(new dat_api<3>(&mod->Mul__state));
  sim_data.signal_map["Mul.state"] = 11;
  sim_data.clk_map["clk"] = new clk_api(&mod->clk);
}
