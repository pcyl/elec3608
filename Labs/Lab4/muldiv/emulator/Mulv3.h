#ifndef __Mulv3__
#define __Mulv3__

#include "emulator.h"

class Mulv3_t : public mod_t {
 private:
  val_t __rand_seed;
  void __srand(val_t seed) { __rand_seed = seed; }
  val_t __rand_val() { return ::__rand_val(&__rand_seed); }
 public:
  dat_t<1> Mulv3__io_store;
  dat_t<1> Mulv3__io_load;
  dat_t<4> Mulv3__io_y;
  dat_t<4> Mulv3_m__io_y;
  dat_t<4> Mulv3__io_x;
  dat_t<4> Mulv3_m__io_x;
  dat_t<9> Mulv3_m__io_z;
  dat_t<9> Mulv3__io_z;
  dat_t<1> Mulv3__io_store__prev;
  dat_t<1> Mulv3__io_load__prev;
  dat_t<4> Mulv3__io_y__prev;
  dat_t<4> Mulv3_m__io_y__prev;
  dat_t<4> Mulv3__io_x__prev;
  dat_t<4> Mulv3_m__io_x__prev;
  dat_t<9> Mulv3_m__io_z__prev;
  dat_t<9> Mulv3__io_z__prev;
  clk_t clk;
  dat_t<1> reset__prev;

  void init ( val_t rand_init = 0 );
  void clock_lo ( dat_t<1> reset, bool assert_fire=true );
  void clock_hi ( dat_t<1> reset );
  int clock ( dat_t<1> reset );
  void print ( FILE* f );
  void print ( std::ostream& s );
  void dump ( FILE* f, val_t t, dat_t<1> reset=LIT<1>(0) );
  void dump_init ( FILE* f );

};

#include "emul_api.h"
class Mulv3_api_t : public emul_api_t {
 public:
  Mulv3_api_t(mod_t* m) : emul_api_t(m) { }
  void init_sim_data();
};

#endif
