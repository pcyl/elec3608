#ifndef __Mux2__
#define __Mux2__

#include "emulator.h"

class Mux2_t : public mod_t {
 private:
  val_t __rand_seed;
  void __srand(val_t seed) { __rand_seed = seed; }
  val_t __rand_val() { return ::__rand_val(&__rand_seed); }
 public:
  dat_t<1> Mux2__io_in0;
  dat_t<1> Mux2__io_sel;
  dat_t<1> Mux2__io_in1;
  dat_t<1> Mux2__io_out;
  clk_t clk;

  void init ( val_t rand_init = 0 );
  void clock_lo ( dat_t<1> reset );
  void clock_hi ( dat_t<1> reset );
  int clock ( dat_t<1> reset );
  void print ( FILE* f );
  void print ( std::ostream& s );
  void dump ( FILE* f, int t );
  void dump_init ( FILE* f );

};



#endif
