// See LICENSE for license details.

//**************************************************************************
// Software multiply function
//--------------------------------------------------------------------------

// Simple C version
int multiply(int x, int y) {
    
    int a = x;
    int b = abs(y);
    int result = 0;
    for (int i = 0; i < b; i++) {
        result += a;
    }
    if (x < 0 && y > 0) {
        return result;
    } else if (x > 0 && y < 0) {
        return -result;
    } else if (x < 0 && y < 0) {
        return -result;
    }
    return result;
}

// Simple assembly version
int multiply_asm(int x, int y);
