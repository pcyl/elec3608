#ifndef __Mul__
#define __Mul__

#include "emulator.h"

class Mul_t : public mod_t {
 private:
  val_t __rand_seed;
  void __srand(val_t seed) { __rand_seed = seed; }
  val_t __rand_val() { return ::__rand_val(&__rand_seed); }
 public:
  dat_t<1> Mul_m__io_load;
  dat_t<1> Mul_m__io_rshift;
  dat_t<1> reset;
  dat_t<1> Mul_m__reset;
  dat_t<1> Mul_m__io_lshift;
  dat_t<1> Mul_m__io_write;
  dat_t<3> T30;
  dat_t<3> Mul__state;
  dat_t<4> Mul__io_y;
  dat_t<4> Mul_m__io_y;
  dat_t<4> T9;
  dat_t<4> Mul_m__multiplier;
  dat_t<4> Mul__io_x;
  dat_t<4> Mul_m__io_x;
  dat_t<8> T16;
  dat_t<8> Mul_m__multiplicand;
  dat_t<8> T28;
  dat_t<8> Mul_m__product;
  dat_t<8> Mul_m__io_z;
  dat_t<8> Mul__io_z;
  dat_t<1> Mul_m__io_load__prev;
  dat_t<1> Mul_m__io_rshift__prev;
  dat_t<1> Mul_m__reset__prev;
  dat_t<1> Mul_m__io_lshift__prev;
  dat_t<1> Mul_m__io_write__prev;
  dat_t<3> Mul__state__prev;
  dat_t<4> Mul__io_y__prev;
  dat_t<4> Mul_m__io_y__prev;
  dat_t<4> Mul_m__multiplier__prev;
  dat_t<4> Mul__io_x__prev;
  dat_t<4> Mul_m__io_x__prev;
  dat_t<8> Mul_m__multiplicand__prev;
  dat_t<8> Mul_m__product__prev;
  dat_t<8> Mul_m__io_z__prev;
  dat_t<8> Mul__io_z__prev;
  clk_t clk;
  dat_t<1> reset__prev;

  void init ( val_t rand_init = 0 );
  void clock_lo ( dat_t<1> reset, bool assert_fire=true );
  void clock_hi ( dat_t<1> reset );
  int clock ( dat_t<1> reset );
  void print ( FILE* f );
  void print ( std::ostream& s );
  void dump ( FILE* f, val_t t, dat_t<1> reset=LIT<1>(0) );
  void dump_init ( FILE* f );

};

#include "emul_api.h"
class Mul_api_t : public emul_api_t {
 public:
  Mul_api_t(mod_t* m) : emul_api_t(m) { }
  void init_sim_data();
};

#endif
