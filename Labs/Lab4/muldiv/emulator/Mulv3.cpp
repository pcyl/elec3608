#include "Mulv3.h"

void Mulv3_t::init ( val_t rand_init ) {
  this->__srand(rand_init);
  clk.len = 1;
  clk.cnt = 0;
  clk.values[0] = 0;
}


int Mulv3_t::clock ( dat_t<1> reset ) {
  uint32_t min = ((uint32_t)1<<31)-1;
  if (clk.cnt < min) min = clk.cnt;
  clk.cnt-=min;
  if (clk.cnt == 0) clock_lo( reset );
  if (!reset.to_bool()) print( std::cerr );
  mod_t::dump( reset );
  if (clk.cnt == 0) clock_hi( reset );
  if (clk.cnt == 0) clk.cnt = clk.len;
  return min;
}


void Mulv3_t::print ( FILE* f ) {
}
void Mulv3_t::print ( std::ostream& s ) {
}


void Mulv3_t::dump_init ( FILE* f ) {
  fputs("$timescale 1ps $end\n", f);
  fputs("$scope module Mulv3 $end\n", f);
  fputs("$var wire 1 \x21 clk $end\n", f);
  fputs("$var wire 1 \x22 reset $end\n", f);
  fputs("$var wire 1 \x23 io_store $end\n", f);
  fputs("$var wire 1 \x24 io_load $end\n", f);
  fputs("$var wire 4 \x25 io_y $end\n", f);
  fputs("$var wire 4 \x27 io_x $end\n", f);
  fputs("$var wire 9 \x2a io_z $end\n", f);
  fputs("$scope module m $end\n", f);
  fputs("$var wire 4 \x26 io_y $end\n", f);
  fputs("$var wire 4 \x28 io_x $end\n", f);
  fputs("$var wire 9 \x29 io_z $end\n", f);
  fputs("$upscope $end\n", f);
  fputs("$upscope $end\n", f);
  fputs("$enddefinitions $end\n", f);
  fputs("$dumpvars\n", f);
  fputs("$end\n", f);
  fputs("#0\n", f);
  if (clk.cnt == 0) {
    clk.values[0] = 1;
    dat_dump<1>(f, clk, 0x21);
  }
  dat_t<1> reset = LIT<1>(1);
  dat_dump<1>(f, reset, 0x22);
  dat_dump<1>(f, Mulv3__io_store, 0x23);
  Mulv3__io_store__prev = Mulv3__io_store;
  dat_dump<1>(f, Mulv3__io_load, 0x24);
  Mulv3__io_load__prev = Mulv3__io_load;
  dat_dump<1>(f, Mulv3__io_y, 0x25);
  Mulv3__io_y__prev = Mulv3__io_y;
  dat_dump<1>(f, Mulv3_m__io_y, 0x26);
  Mulv3_m__io_y__prev = Mulv3_m__io_y;
  dat_dump<1>(f, Mulv3__io_x, 0x27);
  Mulv3__io_x__prev = Mulv3__io_x;
  dat_dump<1>(f, Mulv3_m__io_x, 0x28);
  Mulv3_m__io_x__prev = Mulv3_m__io_x;
  dat_dump<1>(f, Mulv3_m__io_z, 0x29);
  Mulv3_m__io_z__prev = Mulv3_m__io_z;
  dat_dump<1>(f, Mulv3__io_z, 0x2a);
  Mulv3__io_z__prev = Mulv3__io_z;
  fputs("#1\n", f);
  if (clk.cnt == 0) {
    clk.values[0] = 0;
    dat_dump<1>(f, clk, 0x21);
  }
}


void Mulv3_t::dump ( FILE* f, val_t t, dat_t<1> reset ) {
  if (t == 0L) return dump_init(f);
  fprintf(f, "#%lu\n", t << 1);
  if (clk.cnt == 0)  goto L0;
K0:  if (reset != reset__prev)  goto L1;
K1:  if (Mulv3__io_store != Mulv3__io_store__prev)  goto L2;
K2:  if (Mulv3__io_load != Mulv3__io_load__prev)  goto L3;
K3:  if (Mulv3__io_y != Mulv3__io_y__prev)  goto L4;
K4:  if (Mulv3_m__io_y != Mulv3_m__io_y__prev)  goto L5;
K5:  if (Mulv3__io_x != Mulv3__io_x__prev)  goto L6;
K6:  if (Mulv3_m__io_x != Mulv3_m__io_x__prev)  goto L7;
K7:  if (Mulv3_m__io_z != Mulv3_m__io_z__prev)  goto L8;
K8:  if (Mulv3__io_z != Mulv3__io_z__prev)  goto L9;
K9:  fprintf(f, "#%lu\n", (t << 1) + 1);
  if (clk.cnt == 0)  goto Z0;
C0:  return;
L0:
  clk.values[0] = 1;
  dat_dump<1>(f, clk, 0x21);
  goto K0;
L1:
  reset__prev = reset;
  dat_dump<1>(f, reset, 0x22);
  goto K1;
L2:
  Mulv3__io_store__prev = Mulv3__io_store;
  dat_dump<1>(f, Mulv3__io_store, 0x23);
  goto K2;
L3:
  Mulv3__io_load__prev = Mulv3__io_load;
  dat_dump<1>(f, Mulv3__io_load, 0x24);
  goto K3;
L4:
  Mulv3__io_y__prev = Mulv3__io_y;
  dat_dump<1>(f, Mulv3__io_y, 0x25);
  goto K4;
L5:
  Mulv3_m__io_y__prev = Mulv3_m__io_y;
  dat_dump<1>(f, Mulv3_m__io_y, 0x26);
  goto K5;
L6:
  Mulv3__io_x__prev = Mulv3__io_x;
  dat_dump<1>(f, Mulv3__io_x, 0x27);
  goto K6;
L7:
  Mulv3_m__io_x__prev = Mulv3_m__io_x;
  dat_dump<1>(f, Mulv3_m__io_x, 0x28);
  goto K7;
L8:
  Mulv3_m__io_z__prev = Mulv3_m__io_z;
  dat_dump<1>(f, Mulv3_m__io_z, 0x29);
  goto K8;
L9:
  Mulv3__io_z__prev = Mulv3__io_z;
  dat_dump<1>(f, Mulv3__io_z, 0x2a);
  goto K9;
Z0:
  clk.values[0] = 0;
  dat_dump<1>(f, clk, 0x21);
  goto C0;
}




void Mulv3_t::clock_lo ( dat_t<1> reset, bool assert_fire ) {
  { Mulv3_m__io_y.values[0] = Mulv3__io_y.values[0];}
  { Mulv3_m__io_x.values[0] = Mulv3__io_x.values[0];}
  val_t T0;
  T0 = Mulv3_m__io_x.values[0] * Mulv3_m__io_y.values[0];
  val_t T1;
  { T1 = T0 | 0x0L << 8;}
  { Mulv3_m__io_z.values[0] = T1;}
  val_t T2;
  T2 = Mulv3_m__io_x.values[0] * Mulv3_m__io_y.values[0];
  val_t T3;
  { T3 = T2 | 0x0L << 8;}
  { Mulv3__io_z.values[0] = T3;}
}


void Mulv3_t::clock_hi ( dat_t<1> reset ) {
}


void Mulv3_api_t::init_sim_data (  ) {
  sim_data.inputs.clear();
  sim_data.outputs.clear();
  sim_data.signals.clear();
  Mulv3_t* mod = dynamic_cast<Mulv3_t*>(module);
  assert(mod);
  sim_data.inputs.push_back(new dat_api<4>(&mod->Mulv3__io_x));
  sim_data.inputs.push_back(new dat_api<4>(&mod->Mulv3__io_y));
  sim_data.inputs.push_back(new dat_api<1>(&mod->Mulv3__io_load));
  sim_data.inputs.push_back(new dat_api<1>(&mod->Mulv3__io_store));
  sim_data.outputs.push_back(new dat_api<9>(&mod->Mulv3__io_z));
  sim_data.signals.push_back(new dat_api<4>(&mod->Mulv3_m__io_y));
  sim_data.signal_map["Mulv3.m.io_y"] = 0;
  sim_data.signals.push_back(new dat_api<4>(&mod->Mulv3_m__io_x));
  sim_data.signal_map["Mulv3.m.io_x"] = 1;
  sim_data.signals.push_back(new dat_api<9>(&mod->Mulv3_m__io_z));
  sim_data.signal_map["Mulv3.m.io_z"] = 2;
  sim_data.clk_map["clk"] = new clk_api(&mod->clk);
}
