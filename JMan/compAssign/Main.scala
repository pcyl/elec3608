
package compAssign

import Chisel._

object UnitTests {
  def main(args: Array[String]): Unit = {
    val n: Int = 16

    val tutArgs = args.slice(1, args.length)
    args(0) match {
      case "div"=>
        chiselMainTest(tutArgs, () => Module(new divison(n))){ c => new divTester(c, n) }
    }
  }
}
