#include "Mux2.h"

void Mux2_t::init ( val_t rand_init ) {
  this->__srand(rand_init);
  clk.len = 1;
  clk.cnt = clk.len;
  clk.values[0] = 0;
}


int Mux2_t::clock ( dat_t<1> reset ) {
  uint32_t min = ((uint32_t)1<<31)-1;
  if (clk.cnt < min) min = clk.cnt;
  clk.cnt-=min;
  if (clk.cnt == 0) clock_lo( reset );
  if (clk.cnt == 0) clock_hi( reset );
  if (clk.cnt == 0) clk.cnt = clk.len;
  return min;
}


void Mux2_t::print ( FILE* f ) {
}
void Mux2_t::print ( std::ostream& s ) {
}


void Mux2_t::dump_init ( FILE* f ) {
}


void Mux2_t::dump ( FILE* f, int t ) {
}




void Mux2_t::clock_lo ( dat_t<1> reset ) {
  val_t T0;
  { T0 = ~Mux2__io_sel.values[0];}
  T0 = T0 & 0x1L;
  val_t T1;
  { T1 = T0 & Mux2__io_in0.values[0];}
  val_t T2;
  { T2 = Mux2__io_sel.values[0] & Mux2__io_in1.values[0];}
  val_t T3;
  { T3 = T2 | T1;}
  { Mux2__io_out.values[0] = T3;}
}


void Mux2_t::clock_hi ( dat_t<1> reset ) {
}


void Mux2_api_t::init_sim_data (  ) {
  sim_data.inputs.clear();
  sim_data.outputs.clear();
  sim_data.signals.clear();
  Mux2_t* mod = dynamic_cast<Mux2_t*>(module);
  assert(mod);
  sim_data.inputs.push_back(new dat_api<1>(&mod->Mux2__io_sel));
  sim_data.inputs.push_back(new dat_api<1>(&mod->Mux2__io_in0));
  sim_data.inputs.push_back(new dat_api<1>(&mod->Mux2__io_in1));
  sim_data.outputs.push_back(new dat_api<1>(&mod->Mux2__io_out));
  sim_data.clk_map["clk"] = new clk_api(&mod->clk);
}
