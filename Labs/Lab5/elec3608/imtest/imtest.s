	.file	"imtest.c"
	.section	.text.startup,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	add	sp,sp,-32
	li	a0,1
	sw	s2,16(sp)
	sw	s3,12(sp)
	sw	s4,8(sp)
	sw	s5,4(sp)
	sw	s6,0(sp)
	sw	ra,28(sp)
	sw	s0,24(sp)
	sw	s1,20(sp)
	li	s2,1
	call	setStats
	lui	s6,%hi(.LC0)
	lui	s5,%hi(.LC1)
	lui	s4,%hi(.LC2)
	li	s3,6
.L2:
	mv	a1,s2
	add	a0,s6,%lo(.LC0)
	call	printf
	mv	a0,s2
	call	srand
	call	rand
	mv	s0,a0
	sll	s0,s0,16
	call	rand
	or	s0,s0,a0
	call	rand
	mv	s1,a0
	call	rand
	sll	a5,s1,16
	or	a5,a5,a0
	mul	s1,s0,a5
	add	a0,s5,%lo(.LC1)
	add	s2,s2,1
	mulh	a3,s0,a5
	mv	a2,s1
	mulhu	s0,s0,a5
	call	printf
	mv	a2,s1
	add	a0,s4,%lo(.LC2)
	mv	a3,s0
	call	printf
	bne	s2,s3,.L2
	li	a0,0
	call	setStats
	lui	a0,%hi(.LC3)
	li	a1,1
	add	a0,a0,%lo(.LC3)
	call	printf
	li	a0,10
	call	exit
	.size	main, .-main
	.section	.rodata.str1.4,"aMS",@progbits,1
	.align	2
.LC0:
	.string	"iteration %d\n"
	.zero	2
.LC1:
	.string	"resa = %lld\n"
	.zero	3
.LC2:
	.string	"resb = %llu\n"
	.zero	3
.LC3:
	.string	"%d (fail)\n"
	.ident	"GCC: (GNU) 5.3.0"
