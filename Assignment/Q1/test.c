#include <stdio.h>
#include <stdlib.h>
#include "util.h"

#include "multiply.h"

const char* testDiv(int dividend, int divisor, int res, int rem) {
  if (dividend == multiply(res, divisor)+rem) {
    return "True";
  }
  return "False";
}

int main(int argc, char* argv[]) {

  setStats(1);
  printf("================\n");
  for (int i = 0; i < 100; i++) {
    int r1 = rand() % 21 + (-10);
    int r2 = rand() % 21 + (-10);
    int res_rem = r1 % r2;
    int res_div = r1 / r2;

    printf("Iteration: %d\n", i);
    printf("Dividend: %d\n", r1);
    printf("Divisor: %d\n", r2);
    printf("Remainder: %d\n", res_rem);
    printf("Division: %d\n", res_div);
    printf("Correct: %s\n", testDiv(r1,r2,res_div, res_rem));
    printf("================\n");
  }
  setStats(0);

  
  //printf("Hi");
  // exit(0);
}
