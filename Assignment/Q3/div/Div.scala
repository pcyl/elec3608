package Div

import Chisel._
import util.Random

class Divdpath(val n: Int) extends Module {

  val io = new Bundle{
    val x = UInt(INPUT, n)
    val y = UInt(INPUT, n)
    val z = UInt(OUTPUT, 2*n)
    val load = Bool(INPUT)
    val accum = UInt(OUTPUT,n)
  }

  val divisor_m = Reg(init = UInt(0, width = n));
  val dividend_q = Reg(init = UInt(0, width = n));
  val quotient = Reg(init = UInt(0, width = 2 * n));
  val count =  Reg(init = UInt(0, width = n));
  io.accum:=UInt(0)

  when (io.load) {
    dividend_q := io.x
    divisor_m := io.y
    quotient:=(io.x<<UInt(1)) - (io.y << UInt(n))
    count := UInt(n)
  }


  when(count> UInt(0)){
    io.accum := quotient(2*n-1,n)


    when(!quotient(2*n-1)){
      quotient := (quotient<<UInt(1)) - (divisor_m << UInt(n))




    }.elsewhen(quotient(2*n-1)){
      quotient := (quotient<<UInt(1)) + (divisor_m << UInt(n))



    }

    when(!quotient(2*n-1)){
      quotient(0):= quotient(0) | ~quotient(0)
    }.elsewhen(quotient(2*n-1)){
      quotient(0):= quotient(0) & ~quotient(0)
    }

    count := count - UInt(1)

  }

  when(!quotient(2*n-1)){
    io.z := quotient
  }.otherwise{
    io.z := (quotient) + (divisor_m << UInt(n))
  }
}

class Div(val n: Int) extends Module {
  val io= new Bundle{
    val x = UInt(INPUT,16)
    val y = UInt(INPUT,16)
    val z = UInt(OUTPUT,2*16)

    val load=Bool(INPUT)


  }

  val m = Module(new Divdpath(16))

  m.io.x := io.x
  m.io.y := io.y
  m.io.load:=io.load

  io.z:=m.io.z
}

class DivTester(c: Div, n: Int) extends Tester(c) {
  //for (i <- 0 until 10) {
  //  val x = BigInt(n, scala.util.Random)
  //  val y = BigInt(n, scala.util.Random)
  //  poke(c.io.x, x)
  //  poke(c.io.y, y)
  //  step(n * 2)
  //  expect(c.io.z, (x / y))
  //}
  for (i <- 0 until 10) {

    val x = 100
    val y = 4

    poke(c.io.x, x)
    poke(c.io.y, y)
    poke(c.io.load, 1)
    step(1)
    poke(c.io.load, 0)
    step(n)
    expect(c.io.z, (x / y))
 }
}

