#include "divison.h"

void divison_t::init ( val_t rand_init ) {
  this->__srand(rand_init);
  divison_m__count.randomize(&__rand_seed);
  divison_m__divisor_m.randomize(&__rand_seed);
  divison_m__quotient.randomize(&__rand_seed);
  clk.len = 1;
  clk.cnt = 0;
  clk.values[0] = 0;
}


int divison_t::clock ( dat_t<1> reset ) {
  uint32_t min = ((uint32_t)1<<31)-1;
  if (clk.cnt < min) min = clk.cnt;
  clk.cnt-=min;
  if (clk.cnt == 0) clock_lo( reset );
  if (!reset.to_bool()) print( std::cerr );
  mod_t::dump( reset );
  if (clk.cnt == 0) clock_hi( reset );
  if (clk.cnt == 0) clk.cnt = clk.len;
  return min;
}


void divison_t::print ( FILE* f ) {
}
void divison_t::print ( std::ostream& s ) {
}


void divison_t::dump_init ( FILE* f ) {
  fputs("$timescale 1ps $end\n", f);
  fputs("$scope module divison $end\n", f);
  fputs("$var wire 1 \x21 clk $end\n", f);
  fputs("$var wire 1 \x22 reset $end\n", f);
  fputs("$var wire 1 \x23 io_load $end\n", f);
  fputs("$var wire 16 \x27 io_y $end\n", f);
  fputs("$var wire 16 \x2a io_x $end\n", f);
  fputs("$var wire 32 \x2f io_z $end\n", f);
  fputs("$scope module m $end\n", f);
  fputs("$var wire 1 \x24 io_load $end\n", f);
  fputs("$var wire 1 \x25 reset $end\n", f);
  fputs("$var wire 16 \x26 count $end\n", f);
  fputs("$var wire 16 \x28 io_y $end\n", f);
  fputs("$var wire 16 \x29 divisor_m $end\n", f);
  fputs("$var wire 16 \x2b io_x $end\n", f);
  fputs("$var wire 16 \x2c io_accum $end\n", f);
  fputs("$var wire 32 \x2d quotient $end\n", f);
  fputs("$var wire 32 \x2e io_z $end\n", f);
  fputs("$upscope $end\n", f);
  fputs("$upscope $end\n", f);
  fputs("$enddefinitions $end\n", f);
  fputs("$dumpvars\n", f);
  fputs("$end\n", f);
  fputs("#0\n", f);
  if (clk.cnt == 0) {
    clk.values[0] = 1;
    dat_dump<1>(f, clk, 0x21);
  }
  dat_t<1> reset = LIT<1>(1);
  dat_dump<1>(f, reset, 0x22);
  dat_dump<1>(f, divison__io_load, 0x23);
  divison__io_load__prev = divison__io_load;
  dat_dump<1>(f, divison_m__io_load, 0x24);
  divison_m__io_load__prev = divison_m__io_load;
  dat_dump<1>(f, divison_m__reset, 0x25);
  divison_m__reset__prev = divison_m__reset;
  dat_dump<1>(f, divison_m__count, 0x26);
  divison_m__count__prev = divison_m__count;
  dat_dump<1>(f, divison__io_y, 0x27);
  divison__io_y__prev = divison__io_y;
  dat_dump<1>(f, divison_m__io_y, 0x28);
  divison_m__io_y__prev = divison_m__io_y;
  dat_dump<1>(f, divison_m__divisor_m, 0x29);
  divison_m__divisor_m__prev = divison_m__divisor_m;
  dat_dump<1>(f, divison__io_x, 0x2a);
  divison__io_x__prev = divison__io_x;
  dat_dump<1>(f, divison_m__io_x, 0x2b);
  divison_m__io_x__prev = divison_m__io_x;
  dat_dump<1>(f, divison_m__io_accum, 0x2c);
  divison_m__io_accum__prev = divison_m__io_accum;
  dat_dump<1>(f, divison_m__quotient, 0x2d);
  divison_m__quotient__prev = divison_m__quotient;
  dat_dump<1>(f, divison_m__io_z, 0x2e);
  divison_m__io_z__prev = divison_m__io_z;
  dat_dump<1>(f, divison__io_z, 0x2f);
  divison__io_z__prev = divison__io_z;
  fputs("#1\n", f);
  if (clk.cnt == 0) {
    clk.values[0] = 0;
    dat_dump<1>(f, clk, 0x21);
  }
}


void divison_t::dump ( FILE* f, val_t t, dat_t<1> reset ) {
  if (t == 0L) return dump_init(f);
  fprintf(f, "#%lu\n", t << 1);
  if (clk.cnt == 0)  goto L0;
K0:  if (reset != reset__prev)  goto L1;
K1:  if (divison__io_load != divison__io_load__prev)  goto L2;
K2:  if (divison_m__io_load != divison_m__io_load__prev)  goto L3;
K3:  if (divison_m__reset != divison_m__reset__prev)  goto L4;
K4:  if (divison_m__count != divison_m__count__prev)  goto L5;
K5:  if (divison__io_y != divison__io_y__prev)  goto L6;
K6:  if (divison_m__io_y != divison_m__io_y__prev)  goto L7;
K7:  if (divison_m__divisor_m != divison_m__divisor_m__prev)  goto L8;
K8:  if (divison__io_x != divison__io_x__prev)  goto L9;
K9:  if (divison_m__io_x != divison_m__io_x__prev)  goto L10;
K10:  if (divison_m__io_accum != divison_m__io_accum__prev)  goto L11;
K11:  if (divison_m__quotient != divison_m__quotient__prev)  goto L12;
K12:  if (divison_m__io_z != divison_m__io_z__prev)  goto L13;
K13:  if (divison__io_z != divison__io_z__prev)  goto L14;
K14:  fprintf(f, "#%lu\n", (t << 1) + 1);
  if (clk.cnt == 0)  goto Z0;
C0:  return;
L0:
  clk.values[0] = 1;
  dat_dump<1>(f, clk, 0x21);
  goto K0;
L1:
  reset__prev = reset;
  dat_dump<1>(f, reset, 0x22);
  goto K1;
L2:
  divison__io_load__prev = divison__io_load;
  dat_dump<1>(f, divison__io_load, 0x23);
  goto K2;
L3:
  divison_m__io_load__prev = divison_m__io_load;
  dat_dump<1>(f, divison_m__io_load, 0x24);
  goto K3;
L4:
  divison_m__reset__prev = divison_m__reset;
  dat_dump<1>(f, divison_m__reset, 0x25);
  goto K4;
L5:
  divison_m__count__prev = divison_m__count;
  dat_dump<1>(f, divison_m__count, 0x26);
  goto K5;
L6:
  divison__io_y__prev = divison__io_y;
  dat_dump<1>(f, divison__io_y, 0x27);
  goto K6;
L7:
  divison_m__io_y__prev = divison_m__io_y;
  dat_dump<1>(f, divison_m__io_y, 0x28);
  goto K7;
L8:
  divison_m__divisor_m__prev = divison_m__divisor_m;
  dat_dump<1>(f, divison_m__divisor_m, 0x29);
  goto K8;
L9:
  divison__io_x__prev = divison__io_x;
  dat_dump<1>(f, divison__io_x, 0x2a);
  goto K9;
L10:
  divison_m__io_x__prev = divison_m__io_x;
  dat_dump<1>(f, divison_m__io_x, 0x2b);
  goto K10;
L11:
  divison_m__io_accum__prev = divison_m__io_accum;
  dat_dump<1>(f, divison_m__io_accum, 0x2c);
  goto K11;
L12:
  divison_m__quotient__prev = divison_m__quotient;
  dat_dump<1>(f, divison_m__quotient, 0x2d);
  goto K12;
L13:
  divison_m__io_z__prev = divison_m__io_z;
  dat_dump<1>(f, divison_m__io_z, 0x2e);
  goto K13;
L14:
  divison__io_z__prev = divison__io_z;
  dat_dump<1>(f, divison__io_z, 0x2f);
  goto K14;
Z0:
  clk.values[0] = 0;
  dat_dump<1>(f, clk, 0x21);
  goto C0;
}




void divison_t::clock_lo ( dat_t<1> reset, bool assert_fire ) {
  { divison_m__io_load.values[0] = divison__io_load.values[0];}
  val_t T0;
  { T0 = TERNARY(divison_m__io_load.values[0], 0x10L, divison_m__count.values[0]);}
  val_t T1;
  { T1 = divison_m__count.values[0]-0x1L;}
  T1 = T1 & 0xffffL;
  val_t T2;
  T2 = 0x0L<divison_m__count.values[0];
  val_t T3;
  { T3 = TERNARY_1(T2, T1, T0);}
  { divison_m__reset.values[0] = reset.values[0];}
  { T4.values[0] = TERNARY(divison_m__reset.values[0], 0x0L, T3);}
  { divison_m__io_y.values[0] = divison__io_y.values[0];}
  val_t T5;
  { T5 = TERNARY_1(divison_m__io_load.values[0], divison_m__io_y.values[0], divison_m__divisor_m.values[0]);}
  { T6.values[0] = TERNARY(divison_m__reset.values[0], 0x0L, T5);}
  val_t T7;
  T7 = divison_m__io_y.values[0] << 0x10L;
  T7 = T7 & 0xffffffffL;
  { divison_m__io_x.values[0] = divison__io_x.values[0];}
  val_t T8;
  T8 = divison_m__io_x.values[0] << 0x1L;
  T8 = T8 & 0x1ffffL;
  val_t T9;
  { T9 = T8 | 0x0L << 17;}
  val_t T10;
  { T10 = T9-T7;}
  T10 = T10 & 0xffffffffL;
  val_t T11;
  { T11 = TERNARY_1(divison_m__io_load.values[0], T10, divison_m__quotient.values[0]);}
  val_t T12;
  { T12 = T11 | 0x0L << 32;}
  val_t T13;
  T13 = divison_m__divisor_m.values[0] << 0x10L;
  T13 = T13 & 0xffffffffL;
  val_t T14;
  { T14 = T13 | 0x0L << 32;}
  val_t T15;
  T15 = divison_m__quotient.values[0] << 0x1L;
  T15 = T15 & 0x1ffffffffL;
  val_t T16;
  { T16 = T15-T14;}
  T16 = T16 & 0x1ffffffffL;
  val_t T17;
  T17 = (divison_m__quotient.values[0] >> 31) & 1;
  val_t T18;
  { T18 = T17 ^ 0x1L;}
  val_t T19;
  { T19 = T2 & T18;}
  val_t T20;
  { T20 = TERNARY_1(T19, T16, T12);}
  val_t T21;
  T21 = divison_m__divisor_m.values[0] << 0x10L;
  T21 = T21 & 0xffffffffL;
  val_t T22;
  { T22 = T21 | 0x0L << 32;}
  val_t T23;
  T23 = divison_m__quotient.values[0] << 0x1L;
  T23 = T23 & 0x1ffffffffL;
  val_t T24;
  { T24 = T23+T22;}
  T24 = T24 & 0x1ffffffffL;
  val_t T25;
  T25 = (divison_m__quotient.values[0] >> 31) & 1;
  val_t T26;
  { T26 = T18 ^ 0x1L;}
  val_t T27;
  { T27 = T26 & T25;}
  val_t T28;
  { T28 = T2 & T27;}
  val_t T29;
  { T29 = TERNARY_1(T28, T24, T20);}
  val_t T30;
  T30 = (divison_m__quotient.values[0] >> 0) & 1;
  val_t T31;
  { T31 = ~T30;}
  T31 = T31 & 0x1L;
  val_t T32;
  T32 = (divison_m__quotient.values[0] >> 0) & 1;
  val_t T33;
  { T33 = T32 | T31;}
  val_t T34;
  { T34 = TERNARY(T33, 0x3L, 0x0L);}
  val_t T35;
  { T35 = T34 & 0x1L;}
  val_t T36;
  T36 = (T35 >> 1) & 1;
  val_t T37;
  { T37 = TERNARY(T36, 0x7fffffffL, 0x0L);}
  val_t T38;
  { T38 = T35 | T37 << 2;}
  val_t T39;
  { T39 = ~0x1L;}
  T39 = T39 & 0x3L;
  val_t T40;
  T40 = (T39 >> 1) & 1;
  val_t T41;
  { T41 = TERNARY(T40, 0x7fffffffL, 0x0L);}
  val_t T42;
  { T42 = T39 | T41 << 2;}
  val_t T43;
  { T43 = T29 & T42;}
  val_t T44;
  { T44 = T43 | T38;}
  val_t T45;
  T45 = (divison_m__quotient.values[0] >> 31) & 1;
  val_t T46;
  { T46 = T45 ^ 0x1L;}
  val_t T47;
  { T47 = T2 & T46;}
  val_t T48;
  { T48 = TERNARY_1(T47, T44, T29);}
  val_t T49;
  T49 = (divison_m__quotient.values[0] >> 0) & 1;
  val_t T50;
  { T50 = ~T49;}
  T50 = T50 & 0x1L;
  val_t T51;
  T51 = (divison_m__quotient.values[0] >> 0) & 1;
  val_t T52;
  { T52 = T51 & T50;}
  val_t T53;
  { T53 = TERNARY(T52, 0x3L, 0x0L);}
  val_t T54;
  { T54 = T53 & 0x1L;}
  val_t T55;
  T55 = (T54 >> 1) & 1;
  val_t T56;
  { T56 = TERNARY(T55, 0x7fffffffL, 0x0L);}
  val_t T57;
  { T57 = T54 | T56 << 2;}
  val_t T58;
  { T58 = ~0x1L;}
  T58 = T58 & 0x3L;
  val_t T59;
  T59 = (T58 >> 1) & 1;
  val_t T60;
  { T60 = TERNARY(T59, 0x7fffffffL, 0x0L);}
  val_t T61;
  { T61 = T58 | T60 << 2;}
  val_t T62;
  { T62 = T48 & T61;}
  val_t T63;
  { T63 = T62 | T57;}
  val_t T64;
  T64 = (divison_m__quotient.values[0] >> 31) & 1;
  val_t T65;
  { T65 = T46 ^ 0x1L;}
  val_t T66;
  { T66 = T65 & T64;}
  val_t T67;
  { T67 = T2 & T66;}
  val_t T68;
  { T68 = TERNARY_1(T67, T63, T48);}
  val_t T69;
  { T69 = TERNARY(divison_m__reset.values[0], 0x0L, T68);}
  { T70.values[0] = T69;}
  T70.values[0] = T70.values[0] & 0xffffffffL;
  val_t T71;
  { T71 = divison_m__quotient.values[0] >> 16;}
  T71 = T71 & 0xffffL;
  val_t T72;
  { T72 = TERNARY(T2, T71, 0x0L);}
  { divison_m__io_accum.values[0] = T72;}
  val_t T73;
  T73 = divison_m__divisor_m.values[0] << 0x10L;
  T73 = T73 & 0xffffffffL;
  val_t T74;
  { T74 = divison_m__quotient.values[0]+T73;}
  T74 = T74 & 0xffffffffL;
  val_t T75;
  T75 = (divison_m__quotient.values[0] >> 31) & 1;
  val_t T76;
  { T76 = T75 ^ 0x1L;}
  val_t T77;
  { T77 = TERNARY_1(T76, divison_m__quotient.values[0], T74);}
  { divison_m__io_z.values[0] = T77;}
  { divison__io_z.values[0] = divison_m__io_z.values[0];}
}


void divison_t::clock_hi ( dat_t<1> reset ) {
  dat_t<16> divison_m__count__shadow = T4;
  dat_t<16> divison_m__divisor_m__shadow = T6;
  dat_t<32> divison_m__quotient__shadow = T70;
  divison_m__count = T4;
  divison_m__divisor_m = T6;
  divison_m__quotient = T70;
}


void divison_api_t::init_sim_data (  ) {
  sim_data.inputs.clear();
  sim_data.outputs.clear();
  sim_data.signals.clear();
  divison_t* mod = dynamic_cast<divison_t*>(module);
  assert(mod);
  sim_data.inputs.push_back(new dat_api<16>(&mod->divison__io_x));
  sim_data.inputs.push_back(new dat_api<16>(&mod->divison__io_y));
  sim_data.inputs.push_back(new dat_api<1>(&mod->divison__io_load));
  sim_data.outputs.push_back(new dat_api<32>(&mod->divison__io_z));
  sim_data.signals.push_back(new dat_api<1>(&mod->divison_m__io_load));
  sim_data.signal_map["divison.m.io_load"] = 0;
  sim_data.signals.push_back(new dat_api<1>(&mod->divison_m__reset));
  sim_data.signal_map["divison.m.reset"] = 1;
  sim_data.signals.push_back(new dat_api<16>(&mod->divison_m__count));
  sim_data.signal_map["divison.m.count"] = 2;
  sim_data.signals.push_back(new dat_api<16>(&mod->divison_m__io_y));
  sim_data.signal_map["divison.m.io_y"] = 3;
  sim_data.signals.push_back(new dat_api<16>(&mod->divison_m__divisor_m));
  sim_data.signal_map["divison.m.divisor_m"] = 4;
  sim_data.signals.push_back(new dat_api<16>(&mod->divison_m__io_x));
  sim_data.signal_map["divison.m.io_x"] = 5;
  sim_data.signals.push_back(new dat_api<32>(&mod->divison_m__quotient));
  sim_data.signal_map["divison.m.quotient"] = 6;
  sim_data.signals.push_back(new dat_api<16>(&mod->divison_m__io_accum));
  sim_data.signal_map["divison.m.io_accum"] = 7;
  sim_data.signals.push_back(new dat_api<32>(&mod->divison_m__io_z));
  sim_data.signal_map["divison.m.io_z"] = 8;
  sim_data.clk_map["clk"] = new clk_api(&mod->clk);
}
