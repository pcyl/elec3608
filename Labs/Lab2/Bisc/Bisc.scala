package TutorialExamples

import Chisel._
import scala.collection.mutable.ArrayBuffer

object C {
  // memory and datapath sizes
  val WORDLENGTH = 32	// datapath size
  val IADDRZ = 10	// number of bits for instruction memory
}

class ALU extends Module {
  val io = new Bundle {
    val a      = UInt(INPUT,  32)
    val b      = UInt(INPUT,  32)
    val op = UInt(INPUT,  1)
    val out = UInt(OUTPUT, 32)
  }
  io.out := UInt(0)
  when (io.op === UInt(0)) {
    io.out := io.a + io.b //ADD
  } .elsewhen (io.op === UInt(1)) {
    //io.out := Cat(io.a(30,0), UInt(0))
    io.out := io.a << 1
  }
}

class Bisc extends Module {
  val io = new Bundle {
    val isWr   = Bool(INPUT)
    val wrAddr = UInt(INPUT, C.IADDRZ)
    val wrData = Bits(INPUT, C.WORDLENGTH)
    val reset   = Bool(INPUT)
    val iout    = UInt(OUTPUT, C.WORDLENGTH)
    val pcout    = UInt(OUTPUT, C.WORDLENGTH)
  }
  val regfile = Mem(Bits(width = C.WORDLENGTH), 4)
  val imem = Mem(Bits(width = C.WORDLENGTH), 1 << C.IADDRZ)
  val pc   = Reg(init=UInt(0, C.WORDLENGTH))
  val ir   = imem(pc)
  val func = ir(30)
  val rd = ir(29,28)
  val rs1 = ir(27,26)
  val rs2 = ir(25,24)
  val addr = ir(9,0)
  val alu = Module(new ALU())

  alu.io.a := regfile(rs1)
  alu.io.b := regfile(rs2)
  alu.io.op := func

  when (io.isWr) {
    imem(io.wrAddr) := io.wrData
  } .elsewhen (io.reset) {
    pc := UInt(0)
    ir := UInt(0)
    regfile(1) := UInt(1)
  } .elsewhen (ir(31) === UInt(1)) {
    pc := addr
  } .elsewhen (ir(31) === UInt(0)) {
    regfile(rd) := alu.io.out
    pc := pc + UInt(1)
  } .otherwise {
    pc := pc + UInt(1)
  }

  // FILL IN HERE
  io.iout := regfile(rd)
  io.pcout := pc


}

class BiscTests(c: Bisc, q: Int) extends Tester(c) {
  def wr(addr: UInt, data: UInt)  = {
    poke(c.io.isWr,   1)
    poke(c.io.wrAddr, addr.litValue())
    poke(c.io.wrData, data.litValue())
    step(1)
  }
  def reset()  = {
    poke(c.io.isWr, 0)
    poke(c.io.reset, 1)
    peek(c.io.iout)
    step(1)
    poke(c.io.isWr, 0)
    poke(c.io.reset, 0)
    peek(c.io.iout)
    step(1)
  }
  def tick()  = {
    step(1)
    peek(c.io.iout)
    peek(c.io.pcout)
  }
  def StrUInt(s: String) = UInt(BigInt(s, 16))
  val app  = Array(
    StrUInt("0fa40000"),
    StrUInt("27a50004"),
    StrUInt("24a60004"),
    StrUInt("00041080"),
    StrUInt("00c23021"),
    StrUInt("0c100009"),
    StrUInt("00000000"),
    StrUInt("3402000a"),
    StrUInt("0000000c"),
    StrUInt("3c01ffff"),
    StrUInt("343d2000"),
    StrUInt("27bdffe0"),
    StrUInt("8fb00003"),
    StrUInt("afbe0010"),
    StrUInt("27be001c"),
    StrUInt("34100000"),
    StrUInt("34110014"),
    StrUInt("00102021"),
    StrUInt("0c10001c"),
    StrUInt("0002c821"),
    StrUInt("22100001"),
    StrUInt("0211402b"),
    StrUInt("1500fffa"),
    StrUInt("00051021"),
    StrUInt("8fbf0014"),
    StrUInt("8fbe0010"),
    StrUInt("27bd0020"),
    StrUInt("03e00008"),
    StrUInt("27bdffe0"),
    StrUInt("afbf0014"),
    StrUInt("afbe0010"),
    StrUInt("27be001c"),
    StrUInt("afc40000"),
    StrUInt("8fc20000"),
    StrUInt("14400003"),
    StrUInt("34020000"),
    StrUInt("08100034"),
    StrUInt("34080001"),
    StrUInt("0102482b"),
    StrUInt("15200002"),
    StrUInt("08100034"),
    StrUInt("8fc30000"),
    StrUInt("2462ffff"),
    StrUInt("00022021"),
    StrUInt("0c10001c"),
    StrUInt("afc20004"),
    StrUInt("8fc30000"),
    StrUInt("2462fffe"),
    StrUInt("00022021"),
    StrUInt("0c10001c"),
    StrUInt("8fc30004"),
    StrUInt("00431021"),
    StrUInt("8fbf0014"),
    StrUInt("8fbe0010"),
    StrUInt("27bd0020"),
    StrUInt("03e00008"))
  wr(UInt(0), Bits(0)) // skip reset
  for (addr <- 0 until app.length)
    wr(UInt(addr), app(addr))
  reset()
  var k = 0
  do {
    tick(); k += 1
  } while (k < 30)
  if (q == 1) {		// Tutorial part 1
    expect(c.io.pcout, 0x1f)
    expect(c.io.iout, 0x27be001c)
  }
  if (q == 2) {	// Tutorial part 2
    expect(c.io.pcout, 0xb)
    expect(c.io.iout, 0x27bdffe0)
  }
  if (q == 3) {	// Tutorial part 3
    expect(c.io.pcout, 0xb)
    expect(c.io.iout, 0x6)
  }
}

object TutorialExamples {
  def main(args: Array[String]): Unit = {
    val tutArgs = args.slice(1, args.length)
    val res =
    args(0) match {
      case "Bisc" =>
        chiselMainTest(tutArgs, () => Module(new Bisc())){
          c => new BiscTests(c, 3)}
    }
  }
}
