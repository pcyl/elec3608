# Declaration of common variables

RISCV           := /home/riscv/riscvtools
srcDir          := /home/riscv/elec3608/mullab
installTop      := $(DESTDIR)$(RISCV)
buildIncludeDir := $(RISCV)/include
buildLibDir     := $(RISCV)/lib
buildDir        := /home/riscv/elec3608/mullab

# Paths to different source trees
chiseldir       := 

CXX := g++
SBT := java -Xmx4096M -Xss8M -XX:MaxPermSize=128M -jar $(srcDir)/sbt/sbt-launch.jar $(SBT_FLAGS)

