/*
**	blocked matrix multiplication example
**	compile with gcc -O3 -o mm-1 mm-1.c
*/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <strings.h>
#include <time.h>
#include <memory.h>
#include <sys/time.h>
// Additional libraries
#include <pthread.h>
#include <inttypes.h>

/* figures out number of elements in an array a */
#define    NELTS(a) (sizeof(a) / sizeof(a[0]))
/* size of the matrices */
#define    N        1000
/* gives the min/max of a and b */
#define    min(a,b) ((a) < (b) ? (a) : (b))
#define    max(a,b) ((a) > (b) ? (a) : (b))

double __attribute__((aligned(64)))   a[N][N];
double __attribute__((aligned(64)))   b[N][N];
double __attribute__((aligned(64)))   c[N][N];

/*
** standard matrix multiplication
*/
void
mmul0(int b, double C[N][N], double A[N][N], double B[N][N])
{
    int    i, j, k;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            for (k = 0; k < N; k++) {
                C[j][i] += A[j][k] * B[k][i];
            }
        }
    }
}

void
mmul1(int b, double C[N][N], double A[N][N], double B[N][N])
{
    int    i, j, k;

    for (i = 0; i < N; i++) {
        for (k = 0; k < N; k++) {
            for (j = 0; j < N; j++) {
                C[j][i] += A[j][k] * B[k][i];
            }
        }
    }
}

void
mmul2(int b, double C[N][N], double A[N][N], double B[N][N])
{
    int    i, j, k;

    for (j = 0; j < N; j++) {
        for (i = 0; i < N; i++) {
            for (k = 0; k < N; k++) {
                C[j][i] += A[j][k] * B[k][i];
            }
        }
    }
}

void
mmul3(int b, double C[N][N], double A[N][N], double B[N][N])
{
    int    i, j, k;

    for (k = 0; k < N; k++) {
        for (j = 0; j < N; j++) {
            for (i = 0; i < N; i++) {
                C[j][i] += A[j][k] * B[k][i];
            }
        }
    }
}

void
mmul4(int b, double C[N][N], double A[N][N], double B[N][N])
{
    int    i, j, k;

    for (j = 0; j < N; j++) {
        for (k = 0; k < N; k++) {
            for (i = 0; i < N; i++) {
                C[j][i] += A[j][k] * B[k][i];
            }
        }
    }
}

void
mmul5(int b, double C[N][N], double A[N][N], double B[N][N])
{
    int    i, j, k;

    for (k = 0; k < N; k++) {
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++) {
                C[j][i] += A[j][k] * B[k][i];
            }
        }
    }
}

void
mmulb(int b, double C[N][N], double A[N][N], double B[N][N])
{
  for(int jj=0;jj<N;jj+= b){
          for(int kk=0;kk<N;kk+= b){
                  for(int j=0;j<N;j++){
                          for(int k = jj; k<((jj+b)>N?N:(jj+b)); k++){
                                  double temp = 0;
                                  for(int i = kk; i<((kk+b)>N?N:(kk+b)); i++){
                                          temp += A[i][k]*B[k][j];
                                  }
                                  C[j][k] += temp;
                          }
                  }
          }
  }
}

struct argument
{
    // const uint32_t* matA; // 8 bytes
    // const double* __attribute__((aligned(64))) matA;
    // // const uint32_t* matB; // 8 bytes
    // const double* __attribute__((aligned(64))) matB;
    // // uint32_t* result; // 8 bytes
    // double* __attribute__((aligned(64))) result;
    // // uint32_t scalar; // 8 bytes
    // uint32_t __attribute__((aligned(64))) scalar;
    // size_t tid; // 4 bytes
    // char padding[16];
    size_t __attribute__((aligned(64))) tid;
};

static ssize_t g_nthreads = 4;

void *MatrixMul (void * args)
{
    int thread_dimension = N/g_nthreads;
    struct argument arg = *(struct argument *)args;

    size_t tid = arg.tid;
    // sum the values from start to end
    const size_t start = tid * thread_dimension;
    const size_t end =
        tid == g_nthreads - 1 ? N : (tid + 1) * thread_dimension;

    for (size_t y = start; y < end; ++y) {
        for (size_t k = 0; k < N; ++k) {
            for (size_t x = 0; x < N; ++x) {
                c[y][x] += a[y][k] * b[k][x];
            }
        }
    }

    return NULL;
}

void
mmulopt(int b, double C[N][N], double A[N][N], double B[N][N])
{
  pthread_t thread_ids[g_nthreads];

  struct argument *args = malloc(sizeof(struct argument) * g_nthreads);

  for (size_t i = 0; i < g_nthreads; ++i) {
      args[i] = (struct argument) {
          .tid = i
      };
  }

  // launch
  for (size_t i = 0; i < g_nthreads; ++i) {
      if (pthread_create(thread_ids + i, NULL, MatrixMul, args + i) != 0) {
          perror("pthread_create failed");
          exit(1);
      }
  }

  // wait for the all the threads to finish
  for (size_t i = 0; i < g_nthreads; ++i) {
      if (pthread_join(thread_ids[i], NULL) != 0) {
          perror("pthread_join failed");
          exit(1);
      }
  }

  free(args);
}

/*
** this is an array of pointers to the different
** matrix multiplication functions
** (*mtab[i])() calls the ith function
** add your own routines and update this table
*/
void (*mtab[])(int b, double C[N][N], double A[N][N], double B[N][N]) =
    {  mmulb, mmulopt};

/*
** choose the matrix multiplication routine to call
*/
void
mmul(int cmd, int bsize, double C[N][N], double A[N][N], double B[N][N])
{
    /* clear contents of C */
    memset((void *)C, '\0', sizeof(C[0][0]) * N * N);
    (*mtab[cmd])(bsize, c, a, b);
}

/* fill a matrix with random numbers */
void
mfill(double A[N][N])
{
    int i, j;

    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++)
            A[i][j] = (double )rand()/(RAND_MAX+1.0) - 0.5;
}

/* sum all entries of the matrix */
double
msum(double A[N][N])
{
    int    i, j;
    double    sum = 0;

    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++)
            sum += A[i][j];
    return sum;
}

void
runmm(int bsize)
{
    clock_t   start;
    double    elapsed;
    int       i;

    srand(1);    /* initialise seed so we get same result every run */
    mfill(a);
    mfill(b);
    printf("Block size = %d, N = %d\n", bsize, N);
    for (i = 0; i < NELTS(mtab); i++) {
        // start = clock();
        // mmul(i, bsize, c, b, a);
        // elapsed = (double)(clock() - start) / CLOCKS_PER_SEC;
        struct timeval start,end;
        gettimeofday(&start, NULL);
        mmul(i, bsize, c, b, a);
        gettimeofday(&end, NULL);
        elapsed = ((end.tv_sec - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
        printf("--- fn = mmul%d, ", i);
        printf("MFLOPS = %g, elapsed time = %gs, sum = %g\n",
            2.0 * N * N * N / 1.0e6 / elapsed, elapsed, msum(c));
    }
}

usage(char *myname)
{
    (void)fprintf(stderr, "%s [-b bsize]\n", myname);
    exit(1);
}

int
main(int argc, char *argv[])
{
    char *myname = argv[0];
    int ch;
    int vflag = 0, bsize = 50;

    while ((ch = getopt(argc, argv, "b:")) != -1) {
        switch (ch) {
        case 'v':
            vflag = 1;
            break;
        case 'b':
            bsize = atoi(optarg);
            if (bsize < 0) {
                usage(myname);
            }
            break;
        default:
            usage(myname);
        }
    }
    argc -= optind;
    argv += optind;
    runmm(bsize);
    return 0;
}
