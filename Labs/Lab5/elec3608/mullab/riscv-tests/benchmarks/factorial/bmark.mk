#=======================================================================
# UCB CS250 Makefile fragment for benchmarks
#-----------------------------------------------------------------------
#
# Each benchmark directory should have its own fragment which 
# essentially lists what the source files are and how to link them
# into an riscv and/or host executable. All variables should include 
# the benchmark name as a prefix so that they are unique.
#

factorial_c_src = \
	factorial_main.c \
	syscalls.c \

factorial_riscv_src = \
	crt.S \

factorial_c_objs     = $(patsubst %.c, %.o, $(factorial_c_src))
factorial_riscv_objs = $(patsubst %.S, %.o, $(factorial_riscv_src))

factorial_host_bin = factorial.host
$(factorial_host_bin): $(factorial_c_src)
	$(HOST_COMP) $^ -o $(factorial_host_bin)

factorial_riscv_bin = factorial.riscv
$(factorial_riscv_bin): $(factorial_c_objs) $(factorial_riscv_objs)
	$(RISCV_LINK) $(factorial_c_objs) $(factorial_riscv_objs) -o $(factorial_riscv_bin) $(RISCV_LINK_OPTS)

junk += $(factorial_c_objs) $(factorial_riscv_objs) \
        $(factorial_host_bin) $(factorial_riscv_bin)
