#include "Bisc.h"

void Bisc_t::init ( val_t rand_init ) {
  this->__srand(rand_init);
  Bisc__regfile.randomize(&__rand_seed);
  Bisc__pc.randomize(&__rand_seed);
  Bisc__imem.randomize(&__rand_seed);
  clk.len = 1;
  clk.cnt = 0;
  clk.values[0] = 0;
}


int Bisc_t::clock ( dat_t<1> reset ) {
  uint32_t min = ((uint32_t)1<<31)-1;
  if (clk.cnt < min) min = clk.cnt;
  clk.cnt-=min;
  if (clk.cnt == 0) clock_lo( reset );
  if (!reset.to_bool()) print( std::cerr );
  mod_t::dump( reset );
  if (clk.cnt == 0) clock_hi( reset );
  if (clk.cnt == 0) clk.cnt = clk.len;
  return min;
}


void Bisc_t::print ( FILE* f ) {
}
void Bisc_t::print ( std::ostream& s ) {
}


void Bisc_t::dump_init ( FILE* f ) {
  fputs("$timescale 1ps $end\n", f);
  fputs("$scope module Bisc $end\n", f);
  fputs("$var wire 1 \x21 clk $end\n", f);
  fputs("$var wire 1 \x22 reset $end\n", f);
  fputs("$var wire 1 \x24 io_reset $end\n", f);
  fputs("$var wire 1 \x25 io_isWr $end\n", f);
  fputs("$var wire 10 \x26 io_wrAddr $end\n", f);
  fputs("$var wire 32 \x27 io_iout $end\n", f);
  fputs("$var wire 32 \x28 io_pcout $end\n", f);
  fputs("$var wire 32 \x2c pc $end\n", f);
  fputs("$var wire 32 \x2d io_wrData $end\n", f);
  fputs("$scope module alu $end\n", f);
  fputs("$var wire 1 \x23 io_op $end\n", f);
  fputs("$var wire 32 \x29 io_b $end\n", f);
  fputs("$var wire 32 \x2a io_a $end\n", f);
  fputs("$var wire 32 \x2b io_out $end\n", f);
  fputs("$upscope $end\n", f);
  fputs("$upscope $end\n", f);
  fputs("$enddefinitions $end\n", f);
  fputs("$dumpvars\n", f);
  fputs("$end\n", f);
  fputs("#0\n", f);
  if (clk.cnt == 0) {
    clk.values[0] = 1;
    dat_dump<1>(f, clk, 0x21);
  }
  dat_t<1> reset = LIT<1>(1);
  dat_dump<1>(f, reset, 0x22);
  dat_dump<1>(f, Bisc_alu__io_op, 0x23);
  Bisc_alu__io_op__prev = Bisc_alu__io_op;
  dat_dump<1>(f, Bisc__io_reset, 0x24);
  Bisc__io_reset__prev = Bisc__io_reset;
  dat_dump<1>(f, Bisc__io_isWr, 0x25);
  Bisc__io_isWr__prev = Bisc__io_isWr;
  dat_dump<1>(f, Bisc__io_wrAddr, 0x26);
  Bisc__io_wrAddr__prev = Bisc__io_wrAddr;
  dat_dump<1>(f, Bisc__io_iout, 0x27);
  Bisc__io_iout__prev = Bisc__io_iout;
  dat_dump<1>(f, Bisc__io_pcout, 0x28);
  Bisc__io_pcout__prev = Bisc__io_pcout;
  dat_dump<1>(f, Bisc_alu__io_b, 0x29);
  Bisc_alu__io_b__prev = Bisc_alu__io_b;
  dat_dump<1>(f, Bisc_alu__io_a, 0x2a);
  Bisc_alu__io_a__prev = Bisc_alu__io_a;
  dat_dump<1>(f, Bisc_alu__io_out, 0x2b);
  Bisc_alu__io_out__prev = Bisc_alu__io_out;
  dat_dump<1>(f, Bisc__pc, 0x2c);
  Bisc__pc__prev = Bisc__pc;
  dat_dump<1>(f, Bisc__io_wrData, 0x2d);
  Bisc__io_wrData__prev = Bisc__io_wrData;
  fputs("#1\n", f);
  if (clk.cnt == 0) {
    clk.values[0] = 0;
    dat_dump<1>(f, clk, 0x21);
  }
}


void Bisc_t::dump ( FILE* f, val_t t, dat_t<1> reset ) {
  if (t == 0L) return dump_init(f);
  fprintf(f, "#%lu\n", t << 1);
  if (clk.cnt == 0)  goto L0;
K0:  if (reset != reset__prev)  goto L1;
K1:  if (Bisc_alu__io_op != Bisc_alu__io_op__prev)  goto L2;
K2:  if (Bisc__io_reset != Bisc__io_reset__prev)  goto L3;
K3:  if (Bisc__io_isWr != Bisc__io_isWr__prev)  goto L4;
K4:  if (Bisc__io_wrAddr != Bisc__io_wrAddr__prev)  goto L5;
K5:  if (Bisc__io_iout != Bisc__io_iout__prev)  goto L6;
K6:  if (Bisc__io_pcout != Bisc__io_pcout__prev)  goto L7;
K7:  if (Bisc_alu__io_b != Bisc_alu__io_b__prev)  goto L8;
K8:  if (Bisc_alu__io_a != Bisc_alu__io_a__prev)  goto L9;
K9:  if (Bisc_alu__io_out != Bisc_alu__io_out__prev)  goto L10;
K10:  if (Bisc__pc != Bisc__pc__prev)  goto L11;
K11:  if (Bisc__io_wrData != Bisc__io_wrData__prev)  goto L12;
K12:  fprintf(f, "#%lu\n", (t << 1) + 1);
  if (clk.cnt == 0)  goto Z0;
C0:  return;
L0:
  clk.values[0] = 1;
  dat_dump<1>(f, clk, 0x21);
  goto K0;
L1:
  reset__prev = reset;
  dat_dump<1>(f, reset, 0x22);
  goto K1;
L2:
  Bisc_alu__io_op__prev = Bisc_alu__io_op;
  dat_dump<1>(f, Bisc_alu__io_op, 0x23);
  goto K2;
L3:
  Bisc__io_reset__prev = Bisc__io_reset;
  dat_dump<1>(f, Bisc__io_reset, 0x24);
  goto K3;
L4:
  Bisc__io_isWr__prev = Bisc__io_isWr;
  dat_dump<1>(f, Bisc__io_isWr, 0x25);
  goto K4;
L5:
  Bisc__io_wrAddr__prev = Bisc__io_wrAddr;
  dat_dump<1>(f, Bisc__io_wrAddr, 0x26);
  goto K5;
L6:
  Bisc__io_iout__prev = Bisc__io_iout;
  dat_dump<1>(f, Bisc__io_iout, 0x27);
  goto K6;
L7:
  Bisc__io_pcout__prev = Bisc__io_pcout;
  dat_dump<1>(f, Bisc__io_pcout, 0x28);
  goto K7;
L8:
  Bisc_alu__io_b__prev = Bisc_alu__io_b;
  dat_dump<1>(f, Bisc_alu__io_b, 0x29);
  goto K8;
L9:
  Bisc_alu__io_a__prev = Bisc_alu__io_a;
  dat_dump<1>(f, Bisc_alu__io_a, 0x2a);
  goto K9;
L10:
  Bisc_alu__io_out__prev = Bisc_alu__io_out;
  dat_dump<1>(f, Bisc_alu__io_out, 0x2b);
  goto K10;
L11:
  Bisc__pc__prev = Bisc__pc;
  dat_dump<1>(f, Bisc__pc, 0x2c);
  goto K11;
L12:
  Bisc__io_wrData__prev = Bisc__io_wrData;
  dat_dump<1>(f, Bisc__io_wrData, 0x2d);
  goto K12;
Z0:
  clk.values[0] = 0;
  dat_dump<1>(f, clk, 0x21);
  goto C0;
}




void Bisc_t::clock_lo ( dat_t<1> reset, bool assert_fire ) {
  val_t T0;
  { T0 = Bisc__pc.values[0];}
  T0 = T0 & 0x3ffL;
  val_t Bisc__ir;
  { Bisc__ir = Bisc__imem.get(T0, 0);}
  { Bisc__rd.values[0] = Bisc__ir >> 28;}
  Bisc__rd.values[0] = Bisc__rd.values[0] & 0x3L;
  val_t T1;
  { T1 = Bisc__regfile.get(Bisc__rd.values[0], 0);}
  { Bisc__io_iout.values[0] = T1;}
  { Bisc__io_pcout.values[0] = Bisc__pc.values[0];}
  val_t Bisc__rs2;
  { Bisc__rs2 = Bisc__ir >> 24;}
  Bisc__rs2 = Bisc__rs2 & 0x3L;
  val_t T2;
  { T2 = Bisc__regfile.get(Bisc__rs2, 0);}
  { Bisc_alu__io_b.values[0] = T2;}
  val_t Bisc__rs1;
  { Bisc__rs1 = Bisc__ir >> 26;}
  Bisc__rs1 = Bisc__rs1 & 0x3L;
  val_t T3;
  { T3 = Bisc__regfile.get(Bisc__rs1, 0);}
  { Bisc_alu__io_a.values[0] = T3;}
  val_t T4;
  { T4 = Bisc_alu__io_a.values[0]+Bisc_alu__io_b.values[0];}
  T4 = T4 & 0xffffffffL;
  val_t Bisc__func;
  Bisc__func = (Bisc__ir >> 30) & 1;
  { Bisc_alu__io_op.values[0] = Bisc__func;}
  val_t T5;
  T5 = Bisc_alu__io_op.values[0] == 0x0L;
  val_t T6;
  { T6 = TERNARY(T5, T4, 0x0L);}
  val_t T7;
  { T7 = T6 | 0x0L << 32;}
  val_t T8;
  T8 = Bisc_alu__io_a.values[0] << 0x1L;
  T8 = T8 & 0x1ffffffffL;
  val_t T9;
  T9 = Bisc_alu__io_op.values[0] == 0x1L;
  val_t T10;
  { T10 = T5 ^ 0x1L;}
  val_t T11;
  { T11 = T10 & T9;}
  val_t T12;
  { T12 = TERNARY_1(T11, T8, T7);}
  val_t T13;
  { T13 = T12;}
  T13 = T13 & 0xffffffffL;
  { Bisc_alu__io_out.values[0] = T13;}
  val_t T14;
  T14 = (Bisc__ir >> 31) & 1;
  val_t T15;
  T15 = T14 == 0x0L;
  val_t T16;
  T16 = (Bisc__ir >> 31) & 1;
  val_t T17;
  T17 = T16 == 0x1L;
  val_t T18;
  { T18 = Bisc__io_isWr.values[0] | Bisc__io_reset.values[0];}
  val_t T19;
  { T19 = T18 | T17;}
  val_t T20;
  { T20 = T19 ^ 0x1L;}
  { T21.values[0] = T20 & T15;}
  val_t T22;
  { T22 = Bisc__io_isWr.values[0] ^ 0x1L;}
  { T23.values[0] = T22 & Bisc__io_reset.values[0];}
  val_t T24;
  { T24 = TERNARY(T23.values[0], 0x0L, Bisc__pc.values[0]);}
  val_t Bisc__addr;
  { Bisc__addr = Bisc__ir;}
  Bisc__addr = Bisc__addr & 0x3ffL;
  val_t T25;
  { T25 = Bisc__addr | 0x0L << 10;}
  val_t T26;
  { T26 = T18 ^ 0x1L;}
  val_t T27;
  { T27 = T26 & T17;}
  val_t T28;
  { T28 = TERNARY_1(T27, T25, T24);}
  val_t T29;
  { T29 = Bisc__pc.values[0]+0x1L;}
  T29 = T29 & 0xffffffffL;
  val_t T30;
  { T30 = TERNARY_1(T21.values[0], T29, T28);}
  val_t T31;
  { T31 = Bisc__pc.values[0]+0x1L;}
  T31 = T31 & 0xffffffffL;
  val_t T32;
  { T32 = T19 | T15;}
  val_t T33;
  { T33 = T32 ^ 0x1L;}
  val_t T34;
  { T34 = TERNARY_1(T33, T31, T30);}
  { T35.values[0] = TERNARY(reset.values[0], 0x0L, T34);}
  { T36.values[0] = Bisc__pc.values[0];}
  T36.values[0] = T36.values[0] & 0x3ffL;
}


void Bisc_t::clock_hi ( dat_t<1> reset ) {
  { if (T21.values[0]) Bisc__regfile.put(Bisc__rd.values[0], 0, Bisc_alu__io_out.values[0]);}
  { if (T23.values[0]) Bisc__regfile.put(0x1L, 0, 0x1L);}
  dat_t<32> Bisc__pc__shadow = T35;
  { if (T23.values[0]) Bisc__imem.put(T36.values[0], 0, 0x0L);}
  { if (Bisc__io_isWr.values[0]) Bisc__imem.put(Bisc__io_wrAddr.values[0], 0, Bisc__io_wrData.values[0]);}
  Bisc__pc = T35;
}


void Bisc_api_t::init_sim_data (  ) {
  sim_data.inputs.clear();
  sim_data.outputs.clear();
  sim_data.signals.clear();
  Bisc_t* mod = dynamic_cast<Bisc_t*>(module);
  assert(mod);
  sim_data.inputs.push_back(new dat_api<1>(&mod->Bisc__io_isWr));
  sim_data.inputs.push_back(new dat_api<10>(&mod->Bisc__io_wrAddr));
  sim_data.inputs.push_back(new dat_api<32>(&mod->Bisc__io_wrData));
  sim_data.inputs.push_back(new dat_api<1>(&mod->Bisc__io_reset));
  sim_data.outputs.push_back(new dat_api<32>(&mod->Bisc__io_iout));
  sim_data.outputs.push_back(new dat_api<32>(&mod->Bisc__io_pcout));
  sim_data.signals.push_back(new dat_api<2>(&mod->Bisc__rd));
  sim_data.signal_map["Bisc.rd"] = 0;
  sim_data.signals.push_back(new dat_api<32>(&mod->Bisc_alu__io_b));
  sim_data.signal_map["Bisc.alu.io_b"] = 1;
  sim_data.signals.push_back(new dat_api<32>(&mod->Bisc_alu__io_a));
  sim_data.signal_map["Bisc.alu.io_a"] = 2;
  sim_data.signals.push_back(new dat_api<1>(&mod->Bisc_alu__io_op));
  sim_data.signal_map["Bisc.alu.io_op"] = 3;
  sim_data.signals.push_back(new dat_api<32>(&mod->Bisc_alu__io_out));
  sim_data.signal_map["Bisc.alu.io_out"] = 4;
  std::string Bisc__regfile_path = "Bisc.regfile";
  for (size_t i = 0 ; i < 4 ; i++) {
    sim_data.signals.push_back(new dat_api<32>(&mod->Bisc__regfile.contents[i]));
    sim_data.signal_map[Bisc__regfile_path+"["+itos(i,false)+"]"] = 5+i;
  }
  sim_data.signals.push_back(new dat_api<32>(&mod->Bisc__pc));
  sim_data.signal_map["Bisc.pc"] = 9;
  std::string Bisc__imem_path = "Bisc.imem";
  for (size_t i = 0 ; i < 1024 ; i++) {
    sim_data.signals.push_back(new dat_api<32>(&mod->Bisc__imem.contents[i]));
    sim_data.signal_map[Bisc__imem_path+"["+itos(i,false)+"]"] = 10+i;
  }
  sim_data.clk_map["clk"] = new clk_api(&mod->clk);
}
